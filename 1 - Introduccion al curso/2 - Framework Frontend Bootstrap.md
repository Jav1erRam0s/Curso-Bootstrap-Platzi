
# Bootstrap

Es una libreria de framework frontend mas utilizada para el desarrollo web en todo el mundo.
Bootstrap es un framework CSS. Nos van a dar una base para realizar un proyecto web permitiendo flexibilidad en el diseño. Ademas nos van a 
dar una organizacion y una estructura de nuestro HTML, CSS y JavaScript, para que escribamos nuestro html dandole ciertas clases a los 
elementos de estilos de css que provee cada uno de los frameworks. Por eso son conocidos como frameworks frontend.

* Grilla : El componente principal es la grilla. Nos permite organizar el contenido en nuestro sitio web y accediendo a nuestro sitio desde 
distintos dispositivos, este contenido se va a adaptar y se va a ver bien en todos ellos. A esto se lo conoce como [responsive-design].

* Estilos para las fuentes : Ademas los framework traen estilos para las fuentes.

* Componentes visuales pre-armados : Estos componentes nos va a permitir hacer todo mucho mas rapido que si lo hariamos nosotros.

Cualquier duda de uso de los componentes siempre es bueno tener encuenta la documentacion oficial. https://getbootstrap.com
